package com.asprojecteam.comixlife.apiInterfaces;

import com.asprojecteam.comixlife.apiBody.*;
import com.asprojecteam.comixlife.apiResponse.*;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ComixLifeApi {

    @POST("api/")
    Call<SendEmailResponse> sendEmail(@Body SendEmailBody sendEmailBody);

    @POST("api/")
    Call<GetNewsResponse> getNews(@Body GetNewsBody getNewsBody);

    @POST("api/")
    Call<GetPopularResponse> getPopular(@Body GetPopularBody getPopularBody);

    @POST("api/")
    Call<GetPagesResponse> getPages(@Body GetPagesBody getPagesBody);

    @POST("api/")
    Call<GetCatalogResponse> getCatalog(@Body GetCatalogBody getCatalogBody);

    @POST("api/")
    Call<GetNewComicsesResponse> getNewComicses(@Body GetNewComicsesBody getNewComicsesBody);
}
