package com.asprojecteam.comixlife.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.List;

public class sqlHelper {

    SQLiteDatabase db;

    private Context mContext;

    public sqlHelper(Context context) {

        mContext = context;

        db = this.mContext.openOrCreateDatabase("comics_life.db", Context.MODE_PRIVATE,null);
        db.execSQL("create table if not exists comicses (id INTEGER PRIMARY KEY autoincrement, comics_id INTEGER, page INTEGER)");
        db.execSQL("create table if not exists favorite (id INTEGER PRIMARY KEY autoincrement, catalog_id INTEGER, name STRING, name_en STRING, image_url STRING, description STRING, catalog_type STRING, year STRING, parent_id INTEGER, translateBy STRING)");
        db.execSQL("create table if not exists downloads_list (id INTEGER PRIMARY KEY autoincrement, catalog_id INTEGER, url STRING, status STRING)");
        db.close();
    }

    public void SaveComicsPage(Integer page, Integer ComicsId){

        db = this.mContext.openOrCreateDatabase("comics_life.db", Context.MODE_PRIVATE,null);

        Cursor c = db.rawQuery("select page from comicses WHERE comics_id="+ComicsId.toString(),null);

        ContentValues values = new ContentValues();
        values.put("comics_id",ComicsId);
        values.put("page",page);

        if(c.moveToFirst())
            db.update("comicses",values,"comics_id="+ComicsId.toString(),null);
        else
            db.insert("comicses",null,values);

        db.close();
    }

    public Integer GetComicsPage(Integer ComicsId){
        db = this.mContext.openOrCreateDatabase("comics_life.db", Context.MODE_PRIVATE,null);
        Cursor c = db.rawQuery("select page from comicses WHERE comics_id="+ComicsId.toString(),null);
        Integer result;

        result = c.moveToFirst() ? c.getInt(0) : 0;
        db.close();
        return result;
    }

    public void DeleteComicsPageImages(){
        db = this.mContext.openOrCreateDatabase("comics_life.db", Context.MODE_PRIVATE,null);
        db.delete("images","type='page'",null);
        db.close();
    }

    public void addToFavorite(){}
}
