package com.asprojecteam.comixlife.sql;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.asprojecteam.comixlife.classes.Catalog;
import com.asprojecteam.comixlife.globals.MainConfig;
import com.asprojecteam.comixlife.interfaces.ILoadFavorite;

import java.util.ArrayList;
import java.util.List;

public class LoadFavorite extends AsyncTask<Void, Void, Void> {

    private List<Catalog> catalogs;

    MainConfig main;

    private Context taskContext;
    private ILoadFavorite mListener;
    private int sqlResult;

    public LoadFavorite(Context Context, ILoadFavorite Listener) {
        this.mListener = Listener;
        this.taskContext = Context;

        main = (MainConfig) taskContext.getApplicationContext();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... params) {

        SQLiteDatabase db = this.taskContext.openOrCreateDatabase("comics_life.db", Context.MODE_PRIVATE,null);
        Cursor c = db.rawQuery("select * from favorite",null);

        catalogs = new ArrayList<>();

        if (c.moveToFirst()) {
            do {

                Catalog newCatalog = new Catalog(taskContext, c.getInt(1));

                newCatalog.setName(c.getString(2));
                newCatalog.setNameEn(c.getString(3));
                newCatalog.setImageUrl(c.getString(4));
                newCatalog.setDescription(c.getString(5));
                newCatalog.setCatalogType(c.getString(6));
                newCatalog.setYear(c.getString(7));
                newCatalog.setParentId(c.getInt(8));
                newCatalog.setLocalization(c.getString(9));
                newCatalog.setContext(this.taskContext);

                catalogs.add(newCatalog);
                main.addToCatalogList(newCatalog);

                sqlResult = main.OK_FAVORITES;

            } while (c.moveToNext());
        } else sqlResult = main.NO_FAVORITES;


        db.close();

        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);

        mListener.onCompleteLoadFavorite(catalogs,sqlResult);
    }
}