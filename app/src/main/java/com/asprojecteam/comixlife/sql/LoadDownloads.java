package com.asprojecteam.comixlife.sql;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.asprojecteam.comixlife.classes.Downloads;
import com.asprojecteam.comixlife.globals.MainConfig;
import com.asprojecteam.comixlife.interfaces.ILoadDownloads;

import java.util.ArrayList;
import java.util.List;

public class LoadDownloads extends AsyncTask<Void, Void, Void> {

    private List<Downloads> downloadsList;

    MainConfig main;

    private Context taskContext;
    private ILoadDownloads mListener;
    private int sqlResult;

    public LoadDownloads(Context Context, ILoadDownloads Listener) {
        this.mListener = Listener;
        this.taskContext = Context;

        main = (MainConfig) taskContext.getApplicationContext();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... params) {

        SQLiteDatabase db = this.taskContext.openOrCreateDatabase("comics_life.db", Context.MODE_PRIVATE,null);
        Cursor c = db.rawQuery("select * from downloads_list",null);

        downloadsList = new ArrayList<>();

        if (c.moveToFirst()) {
            do {

                Downloads newDonload = new Downloads(
                        c.getInt(0),
                        c.getString(1),
                        c.getString(2)
                );

                downloadsList.add(newDonload);

                sqlResult = main.OK_DOWNLOADS;

            } while (c.moveToNext());
        } else sqlResult = main.NO_DOWNLOADS;

        db.close();

        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);

        mListener.onCompleteLoadDownloads(downloadsList,sqlResult);
    }
}