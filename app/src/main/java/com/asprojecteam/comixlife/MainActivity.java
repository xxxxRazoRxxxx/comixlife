package com.asprojecteam.comixlife;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.asprojecteam.comixlife.comisclife.R;
import com.asprojecteam.comixlife.fragments.FragmentCatalog;
import com.asprojecteam.comixlife.fragments.FragmentDownloads;
import com.asprojecteam.comixlife.fragments.FragmentFavorite;
import com.asprojecteam.comixlife.fragments.FragmentMain;
import com.asprojecteam.comixlife.fragments.FragmentOwner;
import com.asprojecteam.comixlife.fragments.FragmentSend;
import com.asprojecteam.comixlife.fragments.FragmentShare;
import com.asprojecteam.comixlife.globals.MainConfig;
import com.asprojecteam.comixlife.interfaces.OnBackPressedListener;
import com.asprojecteam.comixlife.sql.sqlHelper;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    FragmentMain fragmentMainPage;
    FragmentShare fragmentSharePage;
    FragmentSend fragmentSendPage;
    FragmentCatalog fragmentCatalogPage;
    FragmentFavorite fragmentFavoritePage;
    FragmentOwner fragmentOwnerPage;
    FragmentDownloads fragmentDownloadsPage;

    private MainConfig main;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        main = (MainConfig) getApplicationContext();
        main.AppState = main.APP_OPENED;
        main.db = new sqlHelper(main);

        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics metricsB = new DisplayMetrics();
        display.getMetrics(metricsB);

        main.appWidth = metricsB.widthPixels;
        main.appHeight = metricsB.heightPixels;

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        fragmentMainPage = new FragmentMain();
        fragmentSharePage = new FragmentShare();
        fragmentSendPage = new FragmentSend();
        fragmentCatalogPage = new FragmentCatalog();
        fragmentFavoritePage = new FragmentFavorite();
        fragmentOwnerPage = new FragmentOwner();
        fragmentDownloadsPage = new FragmentDownloads();

        main.fragmentMainPage = fragmentMainPage;

        FragmentTransaction fragmentTrans = getSupportFragmentManager().beginTransaction();
        fragmentTrans.replace(R.id.main_container, fragmentMainPage);
        fragmentTrans.commit();
        main.currentFragment = main.FRAGMENT_MAIN;
    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        OnBackPressedListener backPressedListener = null;
        for (Fragment fragment: fm.getFragments()) {
            if (fragment instanceof OnBackPressedListener) {
                backPressedListener = (OnBackPressedListener) fragment;
                break;
            }
        }

        if (backPressedListener != null) {
            backPressedListener.onBackPressed();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        FragmentTransaction fragmentTrans = getSupportFragmentManager().beginTransaction();

        if (id == R.id.nav_main_page) {
            fragmentTrans.replace(R.id.main_container, fragmentMainPage);
            main.currentFragment = main.FRAGMENT_MAIN;
        } else if (id == R.id.nav_share_page) {
            fragmentTrans.replace(R.id.main_container, fragmentSharePage);
            main.currentFragment = main.FRAGMENT_SHARE;
        } else if (id == R.id.nav_send_page) {
            fragmentTrans.replace(R.id.main_container, fragmentSendPage);
            main.currentFragment = main.FRAGMENT_SEND;
        } else if (id == R.id.nav_catalog_page) {
            fragmentTrans.replace(R.id.main_container, fragmentCatalogPage);
            main.currentFragment = main.FRAGMENT_CATALOG;
        } else if (id == R.id.nav_favorite_page) {
            fragmentTrans.replace(R.id.main_container, fragmentFavoritePage);
            main.currentFragment = main.FRAGMENT_FAVORITE;
        } else if (id == R.id.nav_owner) {
            fragmentTrans.replace(R.id.main_container, fragmentOwnerPage);
            main.currentFragment = main.FRAGMENT_OWNER;
        } else if (id == R.id.nav_downloads_page) {
            fragmentTrans.replace(R.id.main_container, fragmentDownloadsPage);
            main.currentFragment = main.FRAGMENT_DOWNLOADS;
        }
        fragmentTrans.addToBackStack(null);
        fragmentTrans.commit();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
