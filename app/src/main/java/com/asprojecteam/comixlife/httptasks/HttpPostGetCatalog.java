package com.asprojecteam.comixlife.httptasks;

import android.content.Context;
import android.os.AsyncTask;

import com.asprojecteam.comixlife.classes.Catalog;
import com.asprojecteam.comixlife.fragments.FragmentCatalog;
import com.asprojecteam.comixlife.globals.MainConfig;
import com.asprojecteam.comixlife.interfaces.ILoadCatalog;

import java.util.List;


public class HttpPostGetCatalog extends AsyncTask<Void, Void, Void> {

    private List<Catalog> catalogs;

    MainConfig main;

    private Context taskContext;
    private ILoadCatalog mListener;
    private int catalogId;
    private String catalogLimit;
    private Integer httpResult;
    private boolean comics;

    public HttpPostGetCatalog(FragmentCatalog fragmentCatalog, ILoadCatalog Listener, int CatalogId, String Limit) {
        this.mListener = Listener;
        this.catalogId = CatalogId;
        this.catalogLimit = Limit;
        this.taskContext = fragmentCatalog.getContext();
        //this.comics = Comics;

        main = (MainConfig) fragmentCatalog.getContext().getApplicationContext();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... params) {
        try {

            //byte[] data = main.getHttpConnection("type=getCatalog&id="+catalogId+"&limit="+catalogLimit);

            /*Catalog coverCatalog = main.CatalogList.get(catalogId);

            if(coverCatalog != null && coverCatalog.description == null && coverCatalog.id != 0) {
                getCatalogInfo(coverCatalog);
            }


            if(data != null){

                String resultString = new String(data, "UTF-8");

                catalogs = new ArrayList<>();

                try{
                    JSONObject dataJsonObj = new JSONObject(resultString);

                    JSONArray catalogsList = dataJsonObj.getJSONArray("catalogs");

                    for (int i = 0; i < catalogsList.length(); i++) {
                        JSONObject catalog = catalogsList.getJSONObject(i);

                        Catalog newCatalog;


                        if(catalog.length()<2)
                            newCatalog = new Catalog(catalog.getString("catalog_type"));
                        else
                            newCatalog = new Catalog(taskContext,
                                    catalog.getInt("catalog_id"),
                                    catalog.getString("name"),
                                    catalog.getString("name_en"),
                                    catalog.getString("image_url"),
                                    catalog.getString("description"),
                                    catalog.getString("catalog_type"),
                                    catalog.getString("year"),
                                    catalog.getInt("parent_id"),
                                    catalog.getString("localization"));

                        catalogs.add(newCatalog);
                    }

                    httpResult = main.HTTP_OK;

                } catch (JSONException e){

                    e.printStackTrace();
                    httpResult = main.HTTP_TIMEOUT_ERROR;

                }

            } else httpResult = main.HTTP_TIMEOUT_ERROR;
*/
        } catch (Exception e) {

            e.printStackTrace();
            httpResult = main.HTTP_TIMEOUT_ERROR;

        }

        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);

        /*if(httpResult != null && mListener != null) {
            mListener.onCompleteLoadCatalog(catalogs,httpResult);
        }*/
    }

    private void getCatalogInfo(Catalog coverCatalog){
        try {

            /*byte[] catalogInfo = main.getHttpConnection("type=getCatalogInfo&id=" + catalogId);

            if (catalogInfo != null) {

                String resultString = new String(catalogInfo, "UTF-8");

                JSONObject dataJsonObj;

                try {
                    dataJsonObj = new JSONObject(resultString);

                    JSONArray info = dataJsonObj.getJSONArray("info");
                    JSONObject comics = info.getJSONObject(0);

                    /*coverCatalog.description = comics.getString("description");
                    coverCatalog.name = comics.getString("name");
                    coverCatalog.nameEn = comics.getString("name_en");
                    coverCatalog.catalogType = comics.getString("catalog_type");
                    coverCatalog.year = comics.getString("year");
                    coverCatalog.image = comics.getString("image_url");
                    coverCatalog.translateBy = comics.getString("localization");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }*/
        } catch (Exception e){ }
    }
}