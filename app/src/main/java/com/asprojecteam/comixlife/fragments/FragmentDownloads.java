package com.asprojecteam.comixlife.fragments;

import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.asprojecteam.comixlife.classes.Downloads;
import com.asprojecteam.comixlife.comisclife.R;
import com.asprojecteam.comixlife.globals.MainConfig;
import com.asprojecteam.comixlife.interfaces.ILoadDownloads;
import com.asprojecteam.comixlife.interfaces.OnBackPressedListener;

import java.util.List;


public class FragmentDownloads extends Fragment implements ILoadDownloads, OnBackPressedListener {

    MainConfig main;

    private ActionBar appBar;
    private RecyclerView DownloadsViewer;

    RelativeLayout emptyDownloads;

    public FragmentDownloads() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        main = (MainConfig) getActivity().getApplicationContext();
        appBar = ((AppCompatActivity)getActivity()).getSupportActionBar();

        View view = inflater.inflate(R.layout.fragment_downloads, container, false);

        emptyDownloads = view.findViewById(R.id.emptyDownloads);

        appBar.setTitle(getResources().getString(R.string.app_name));

        DownloadsViewer = view.findViewById(R.id.downloads_list);
        DownloadsViewer.setHasFixedSize(true);

        LinearLayoutManager llm = new LinearLayoutManager(view.getContext());
        DownloadsViewer.setLayoutManager(llm);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        main.loadDownloads(this.getContext(),this);
    }

    @Override
    public void onCompleteLoadDownloads(List<Downloads> DownloadsList, int Result) {

        main.showMessage(DownloadsList.size());
        /*if(Result != main.NO_FAVORITES){

            showCatalog(Catalogs);

        } else showEmptyError();
        */
    }

    @Override
    public Boolean onBackPressed() {

        main.openMainPage(getActivity());
        main.currentFragment = main.FRAGMENT_MAIN;
        return true;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
