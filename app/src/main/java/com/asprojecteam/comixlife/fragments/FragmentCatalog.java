package com.asprojecteam.comixlife.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.asprojecteam.comixlife.adapters.RVAdapter;
import com.asprojecteam.comixlife.classes.Comics;
import com.asprojecteam.comixlife.comisclife.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import com.asprojecteam.comixlife.classes.Catalog;
import com.asprojecteam.comixlife.globals.MainConfig;
import com.asprojecteam.comixlife.interfaces.OnBackPressedListener;
import com.asprojecteam.comixlife.sql.sqlHelper;

import java.util.List;

import static com.asprojecteam.comixlife.fragments.ReadComicsFragment.comicsId;

public class FragmentCatalog extends Fragment implements OnBackPressedListener {

    private RelativeLayout loadBar;
    public boolean inLoad = false;
    public boolean inRead = false;
    public boolean inLoadComicsInfo = false;

    private Catalog rootCatalog;

    private ActionBar appBar;

    public int publishId;

    private Comics comicsRead;

    MainConfig main;

    private RecyclerView CatalogViewer;
    private View RootView;
    private CoordinatorLayout comicsLayout;
    private RelativeLayout normalLayout;

    ViewPager pager;
    PagerAdapter pagerAdapter;
    SeekBar ReadSeekBar;
    RVAdapter rvaAdapter;

    RelativeLayout httpError;
    Button httpErrorButton;

    RelativeLayout emptyCatalog;

    private sqlHelper database;

    public FragmentCatalog() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        showLoadBar();

        inLoad = true;

        if(main.openNewComics != -1){
            Catalog catalog = new Catalog(getContext(), main.openNewComics);
            catalog.loadComicsPages(this, main.READ_FROM_START);
            hideLoadBar();
        } else if((main.currentCatalogId <= 0 && main.openPopularComics == -1) || main.currentFragment == main.FRAGMENT_CATALOG){

            main.currentCatalogId = -1;
            main.openPopularComics = -1;

            if(main.CatalogList.containsValue(0)) {
                rvaAdapter = null;
                main.CatalogList.remove(0);
            }

            rootCatalog = new Catalog(getContext(), 0);

            main.addToCatalogList(rootCatalog);

            rootCatalog.loadChildren(this);

        } else {

            if(main.openPopularComics != -1){

                Catalog catalog = new Catalog(getContext(), main.openPopularComics);
                main.addToCatalogList(catalog);
                catalog.loadChildren(this);
                return;
            }

            hideLoadBar();

            if(!inRead)
                restoreCurrentState(main.currentCatalogId);

            inLoad = false;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        main = (MainConfig) getActivity().getApplicationContext();

        View view = inflater.inflate(R.layout.fragment_catalog, container, false);

        RootView = view;

        appBar = ((AppCompatActivity)getActivity()).getSupportActionBar();

        emptyCatalog = (RelativeLayout) view.findViewById(R.id.emptyCatalog);
        httpError = (RelativeLayout) view.findViewById(R.id.httpError);
        httpErrorButton = (Button) view.findViewById(R.id.httpErrorButton);
        httpErrorButton.setOnClickListener(getOnClickHttpErrorButton(this));

        database = new sqlHelper(view.getContext());

        ReadSeekBar = (SeekBar) view.findViewById(R.id.read_seekBar);

        pager = (ViewPager) view.findViewById(R.id.pager);
        pager.setAdapter(null);

        comicsLayout = view.findViewById(R.id.comicsLayout);
        normalLayout = view.findViewById(R.id.normalLayout);

        loadBar = view.findViewById(R.id.progressBar_loading_catalog);

        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public View.OnClickListener getOnClickHttpErrorButton(final FragmentCatalog iLoadCatalog){
        View.OnClickListener OnClickCatalogElement = new View.OnClickListener(){
            @Override
            public void onClick(View v)
            {
                showLoadBar();
                hideError();
                main.loadCatalog(iLoadCatalog,main.currentCatalogId);
            }
        };
        return OnClickCatalogElement;
    }

    private class ReadComicsPagerAdapter extends FragmentStatePagerAdapter {

        private int Id;
        private SeekBar readSeekBar;
        private Comics comics;

        public ReadComicsPagerAdapter(FragmentManager fm, int Id, ViewPager Pager, SeekBar ReadSeekBar, Comics Comics) {
            super(fm);
            this.Id = Id;
            this.readSeekBar = ReadSeekBar;
            this.comics = Comics;
            this.comics.pager = Pager;
        }

        @Override
        public Fragment getItem(int position) {
            return ReadComicsFragment.newInstance(pager,position,comicsRead,this.Id,this.readSeekBar);
        }

        @Override
        public int getCount() {
            return comics.pages.length;
        }

    }

    public void setNormalLayout(){
        normalLayout.setVisibility(View.VISIBLE);
        comicsLayout.setVisibility(View.GONE);
        CatalogViewer = RootView.findViewById(R.id.n_favorites_catalog);
        CatalogViewer.setHasFixedSize(true);

        LinearLayoutManager llm = new LinearLayoutManager(RootView.getContext());

        appBar.setTitle(getResources().getString(R.string.app_name));

        CatalogViewer.setLayoutManager(llm);
    }

    public void setComicsLayout(){
        normalLayout.setVisibility(View.GONE);
        comicsLayout.setVisibility(View.VISIBLE);
        CatalogViewer = RootView.findViewById(R.id.c_favorites_catalog);
        CatalogViewer.setHasFixedSize(true);

        LinearLayoutManager llm = new LinearLayoutManager(RootView.getContext());
        ImageView comicsCover = RootView.findViewById(R.id.comicsCover);

        Catalog catalog;

        if(main.openPopularComics == -1)
            catalog = main.CatalogList.get(main.currentCatalogId);
        else
            catalog = main.CatalogList.get(main.openPopularComics);

        catalog.setContext(getContext());

        appBar.setTitle(catalog.getName());

        TextView coverYear = RootView.findViewById(R.id.comicsCover_year);
        TextView coverTranslation = RootView.findViewById(R.id.comicsCover_translation);
        TextView coverDescription = RootView.findViewById(R.id.comicsCover_description);
        ImageButton coverFavoriteBtn = RootView.findViewById(R.id.comicsCover_favorite_button);
        ImageButton coverDownloadBtn = RootView.findViewById(R.id.comicsCover_download_button);

        Glide.with(this)
                .load(catalog.getImageUrl())
                .apply(new RequestOptions()
                    .placeholder(R.drawable.ic_no_picture))
                .into(comicsCover);

        coverYear.setText("Год выпуска: "+catalog.getYear());
        coverTranslation.setText("Переведено: "+catalog.getLocalization());
        coverDescription.setText(catalog.getDescription());
        coverFavoriteBtn.setVisibility(View.VISIBLE);

        if(catalog.checkFavorite())
            coverFavoriteBtn.setImageResource(R.drawable.icon_favorite);
        else
            coverFavoriteBtn.setImageResource(R.drawable.icon_favorite_border);

        if(catalog.isInDownloadList())
            coverDownloadBtn.setVisibility(View.GONE);

        coverFavoriteBtn.setOnClickListener(catalog.onClickFavoriteButton(coverFavoriteBtn));
        coverDownloadBtn.setOnClickListener(catalog.onClickDownloadButton(coverDownloadBtn));

        CatalogViewer.setLayoutManager(llm);
    }

    @Override
    public Boolean onBackPressed() {

        if(!inRead){

            if(!appBar.getTitle().equals(getResources().getString(R.string.app_name)))
                appBar.setTitle(getResources().getString(R.string.app_name));

            if(main.openNewComics != -1) {
                main.openNewComics = -1;
                main.openMainPage(getActivity());
                main.currentFragment = main.FRAGMENT_MAIN;
            } else if(main.openPopularComics != -1){
                main.currentCatalogId = -1;
                main.openPopularComics = -1;

                getFragmentManager().popBackStack();
            }else if(main.currentCatalogId <= 0 ){
                rvaAdapter = null;
                main.currentCatalogId = -1;
                main.openMainPage(getActivity());
                main.currentFragment = main.FRAGMENT_MAIN;
            }
            else if(main.currentCatalogId > 0 ){
                restoreCurrentState(main.currentCatalogId);
                return true;
            }
        } else if(inRead){

            closeComicsPages();
            inRead = false;

            if(main.openNewComics != -1){
                main.openNewComics = -1;
                main.openMainPage(getActivity());
                main.currentFragment = main.FRAGMENT_MAIN;
            }
        }
        return true;
    }

    private void restoreCurrentState(int restoredCatalogId){

        if(restoredCatalogId == main.currentCatalogId && main.openPopularComics == -1){
            setNormalLayout();

            Catalog currentCatalog = main.CatalogList.get(main.currentCatalogId);
            Catalog previousCatalog = main.CatalogList.get(currentCatalog.getParentId());

            rvaAdapter = new RVAdapter(previousCatalog.getChildren(), this, CatalogViewer);
            CatalogViewer.setAdapter(rvaAdapter);
            CatalogViewer.getLayoutManager().onRestoreInstanceState(previousCatalog.getSavedInstanceState());

            main.currentCatalogId = previousCatalog.getCatalogId();
            main.previousCatalogId = previousCatalog.getParentId();
        } else {
            setComicsLayout();

            Catalog restoredCatalog = main.CatalogList.get(restoredCatalogId);
            rvaAdapter = new RVAdapter(restoredCatalog.getChildren(), this, CatalogViewer);
            CatalogViewer.setAdapter(rvaAdapter);
            CatalogViewer.getLayoutManager().onRestoreInstanceState(restoredCatalog.getSavedInstanceState());
        }

        hideLoadBar();
        hideError();
    }

    public void showCatalog(List<Catalog> Catalogs){

        main.addToCatalogList(Catalogs);

        Catalog parentCatalog = main.CatalogList.get(Catalogs.get(0).getParentId());
        parentCatalog.addChildren(Catalogs,rvaAdapter);

        if(rvaAdapter == null || rvaAdapter.catalogId != parentCatalog.getCatalogId() ||  rvaAdapter.catalogId == 0){

            if(main.openPopularComics != -1 || (parentCatalog.getCatalogId() != 0 && Catalogs.get(0).getCatalogType().equals("comics")))
                setComicsLayout();
            else
                setNormalLayout();

            rvaAdapter = new RVAdapter(Catalogs, this, CatalogViewer);
            CatalogViewer.setAdapter(rvaAdapter);

            if(main.openPopularComics == -1) {
                parentCatalog.createChildren(Catalogs);
                parentCatalog.setSavedInstanceState(CatalogViewer.getLayoutManager().onSaveInstanceState());
            }
        }
    }

    public void openComicsPages(Comics comics,int Id, int startType){

        comicsRead = comics;

        ReadSeekBar.setVisibility(View.VISIBLE);
        ReadSeekBar.setMax(comicsRead.pages.length - 1);
        ReadSeekBar.setOnSeekBarChangeListener(comicsRead.getReadSeekBarChangeListner());
        pagerAdapter = new ReadComicsPagerAdapter(getChildFragmentManager(), Id, pager, ReadSeekBar, comicsRead);
        pager.setOffscreenPageLimit(main.COMICS_OFFSCREEN_PAGE_LIMIT);
        pager.setAdapter(pagerAdapter);
        pager.addOnPageChangeListener(comicsRead.getPageChangeListener(ReadSeekBar));
        pager.setBackgroundResource(R.color.colorWhite);

        //Прячим ActionBar
        ((AppCompatActivity)getActivity()).getSupportActionBar().hide();

        //Дочитали до конца -> начинаем с начала

        Integer page = database.GetComicsPage(Id);
        if (page == (comicsRead.pages.length - 1) || startType == main.READ_FROM_START)
            pager.setCurrentItem(0);
        else {
            ReadSeekBar.setProgress(page);
            pager.setCurrentItem(database.GetComicsPage(Id));
        }
    }

    private void closeComicsPages(){
        //Сохраняем страницу, на которой остановились
        database.SaveComicsPage(pager.getCurrentItem(),comicsId);

        //Отображаем ActionBar
        ((AppCompatActivity)getActivity()).getSupportActionBar().show();

        //Останавливаем загрузку комикса
        main.closeLoadComicsPages();

        ReadSeekBar.setVisibility(View.GONE);
        pager.setAdapter(null);
        pager.setBackgroundResource(0);
    }

    public void showEmptyError(){
        emptyCatalog.setVisibility(View.VISIBLE);
    }

    public void hideError(){
        httpError.setVisibility(View.GONE);
        emptyCatalog.setVisibility(View.GONE);
    }

    public void showError(){
        httpError.setVisibility(View.VISIBLE);
        loadBar.setVisibility(View.GONE);
    }

    private void showLoadBar(){
        loadBar.setVisibility(View.VISIBLE);
    }

    public void hideLoadBar(){
        loadBar.setVisibility(View.GONE);
    }
}
