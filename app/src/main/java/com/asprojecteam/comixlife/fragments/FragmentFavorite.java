package com.asprojecteam.comixlife.fragments;

import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.asprojecteam.comixlife.adapters.RVAdapter;
import com.asprojecteam.comixlife.classes.Catalog;
import com.asprojecteam.comixlife.comisclife.R;
import com.asprojecteam.comixlife.globals.MainConfig;
import com.asprojecteam.comixlife.interfaces.ILoadFavorite;
import com.asprojecteam.comixlife.interfaces.OnBackPressedListener;

import java.util.List;

public class FragmentFavorite extends Fragment implements ILoadFavorite, OnBackPressedListener {

    private OnFragmentInteractionListener mListener;

    private RelativeLayout loadBar;
    private ActionBar appBar;

    private RecyclerView CatalogViewer;

    public boolean inLoad = false;

    MainConfig main;
    RVAdapter rvaAdapter;

    RelativeLayout emptyCatalog;

    public FragmentFavorite() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        main = (MainConfig) getActivity().getApplicationContext();

        View view = inflater.inflate(R.layout.fragment_favorite, container, false);

        emptyCatalog = (RelativeLayout) view.findViewById(R.id.emptyFavorite);

        loadBar = view.findViewById(R.id.progressBar_loading_catalog);
        appBar = ((AppCompatActivity)getActivity()).getSupportActionBar();

        appBar.setTitle(getResources().getString(R.string.app_name));

        CatalogViewer = view.findViewById(R.id.favorites_catalog);
        CatalogViewer.setHasFixedSize(true);

        LinearLayoutManager llm = new LinearLayoutManager(view.getContext());
        CatalogViewer.setLayoutManager(llm);

        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        showLoadBar();

        main.loadFavorites(this.getContext(),this);
    }

    @Override
    public void onCompleteLoadFavorite(List<Catalog> Catalogs, int Result) {

        hideLoadBar();

        if(Result != main.NO_FAVORITES){
            //main.addToCatalogList(Catalogs);
            showCatalog(Catalogs);

        } else showEmptyError();

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public Boolean onBackPressed() {
        main.openMainPage(getActivity());
        main.currentFragment = main.FRAGMENT_MAIN;
        return true;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void showCatalog(List<Catalog> Catalogs){
        rvaAdapter = new RVAdapter(Catalogs, this, CatalogViewer);
        CatalogViewer.setAdapter(rvaAdapter);

    }

    private void showEmptyError(){
        emptyCatalog.setVisibility(View.VISIBLE);
    }

    private void showLoadBar(){
        inLoad = true;
        loadBar.setVisibility(View.VISIBLE);
    }

    private void hideLoadBar(){
        inLoad = false;
        loadBar.setVisibility(View.GONE);
    }

}
