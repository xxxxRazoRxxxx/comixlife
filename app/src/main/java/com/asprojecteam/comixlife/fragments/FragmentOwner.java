package com.asprojecteam.comixlife.fragments;

import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.asprojecteam.comixlife.comisclife.R;
import com.asprojecteam.comixlife.globals.MainConfig;
import com.asprojecteam.comixlife.interfaces.OnBackPressedListener;


public class FragmentOwner extends Fragment implements OnBackPressedListener {

    MainConfig main;

    public FragmentOwner() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        main = (MainConfig) getActivity().getApplicationContext();
        View view = inflater.inflate(R.layout.fragment_owner, container, false);
        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public Boolean onBackPressed() {
        main.openMainPage(getActivity());
        main.currentFragment = main.FRAGMENT_MAIN;
        return true;
    }
}
