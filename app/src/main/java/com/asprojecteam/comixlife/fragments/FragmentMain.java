package com.asprojecteam.comixlife.fragments;

import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.asprojecteam.comixlife.adapters.NewComicsesAdapter;
import com.asprojecteam.comixlife.adapters.RVAdapterPopular;
import com.asprojecteam.comixlife.apiBody.GetNewComicsesBody;
import com.asprojecteam.comixlife.apiBody.GetPopularBody;
import com.asprojecteam.comixlife.apiResponse.GetNewComicsesResponse;
import com.asprojecteam.comixlife.apiResponse.GetPopularResponse;
import com.asprojecteam.comixlife.classes.Catalog;
import com.asprojecteam.comixlife.comisclife.R;
import com.asprojecteam.comixlife.globals.MainConfig;
import com.asprojecteam.comixlife.interfaces.OnBackPressedListener;
import com.asprojecteam.comixlife.sql.sqlHelper;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentMain extends Fragment implements OnBackPressedListener {

    private OnFragmentInteractionListener mListener;

    boolean newComicsesLoaded = false;
    boolean catalogLoaded = false;

    RVAdapterPopular rvaAdapter;
    NewComicsesAdapter newComicsesAdapter;

    private RecyclerView catalogViewer;
    private RecyclerView newComicsesViewer;

    private RelativeLayout errorLayoutMain;
    private RelativeLayout httpErrorMain;
    private ProgressBar loadBarMain;
    private Button httpErrorButton;
    private ActionBar appBar;

    MainConfig main;

    private sqlHelper database;

    public FragmentMain() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_main, container, false);

        main = (MainConfig) getActivity().getApplicationContext();

        LinearLayoutManager catalogLLM = new LinearLayoutManager(view.getContext());

        catalogViewer = view.findViewById(R.id.main_catalog);
        catalogViewer.setLayoutManager(catalogLLM);

        LinearLayoutManager newComicsesLLM = new LinearLayoutManager(view.getContext(), LinearLayoutManager.HORIZONTAL, false);

        newComicsesViewer = view.findViewById(R.id.main_new_comicses);
        newComicsesViewer.setLayoutManager(newComicsesLLM);

        database = new sqlHelper(view.getContext());

        ////////////////////////////////////////////////////////////////////////////////////////
        ////
        ////                       Переменные для страницы ошибки
        ////
        ////////////////////////////////////////////////////////////////////////////////////////

        errorLayoutMain = view.findViewById(R.id.ErrorLayoutMain);
        httpErrorMain = view.findViewById(R.id.httpErrorMain);
        loadBarMain = view.findViewById(R.id.loadBarMain);
        httpErrorButton = (Button) view.findViewById(R.id.httpErrorButton);
        httpErrorButton.setOnClickListener(getOnClickHttpErrorButton());

        appBar = ((AppCompatActivity)getActivity()).getSupportActionBar();

        appBar.setTitle(getResources().getString(R.string.app_name));



        // Inflate the layout for this fragment
        return view;
    }


    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ////////////////////////////////////////////////////////////////////////////////////////
        ////
        ////                      Каталог популярных комиксов
        ////
        ////////////////////////////////////////////////////////////////////////////////////////

        showLoadBar();
        loadPopularCatalog();
        loadNewComicses();
    }

    @Override
    public Boolean onBackPressed() {
        main.exitApp(getActivity());
        return true;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public View.OnClickListener getOnClickHttpErrorButton(){
        View.OnClickListener OnClickCatalogElement = new View.OnClickListener(){
            @Override
            public void onClick(View v)
            {
                showLoadBar();
                loadPopularCatalog();
            }
        };
        return OnClickCatalogElement;
    }

    private void loadPopularCatalog(){

        final FragmentMain fragment = this;

        if(main.popularList == null) {

            GetPopularBody getPopularBody = new GetPopularBody(main.getPopularCatalogLimit());

            main.getApi().getPopular(getPopularBody).enqueue(new Callback<GetPopularResponse>() {
                @Override
                public void onResponse(Call<GetPopularResponse> call, Response<GetPopularResponse> response) {
                    if(response.isSuccessful() && response.body() != null){

                        List<Catalog> popularList = response.body().getNewCatalogs();

                        main.addToCatalogList(popularList);

                        if(newComicsesLoaded && catalogLoaded)
                            hideLoadBar();

                        hideError();

                        main.popularList = popularList;

                        rvaAdapter = new RVAdapterPopular(popularList, fragment, catalogViewer);
                        catalogViewer.setAdapter(rvaAdapter);

                    } else {
                        hideLoadBar();
                        showError();
                    }
                }

                @Override
                public void onFailure(Call<GetPopularResponse> call, Throwable t) {
                    hideLoadBar();
                    showError();
                }
            });
        } else {
            rvaAdapter = new RVAdapterPopular(main.popularList, this,catalogViewer);
            catalogViewer.setAdapter(rvaAdapter);
            hideError();
            hideLoadBar();
        }
    }

    private void loadNewComicses(){

        final FragmentMain fragment = this;

        if(main.newComicsesList == null) {
            GetNewComicsesBody getNewComicsesBody = new GetNewComicsesBody(main.NEW_COMICSES_LIMIT);

            main.getApi().getNewComicses(getNewComicsesBody).enqueue(new Callback<GetNewComicsesResponse>() {
                @Override
                public void onResponse(Call<GetNewComicsesResponse> call, Response<GetNewComicsesResponse> response) {
                    if(response.isSuccessful() && response.body() != null){

                        main.newComicsesList = response.body().getNewComicses();
                        if(newComicsesLoaded && catalogLoaded)
                            hideLoadBar();

                        newComicsesAdapter = new NewComicsesAdapter(main.newComicsesList, fragment, newComicsesViewer);
                        newComicsesViewer.setAdapter(newComicsesAdapter);

                        hideError();

                    } else {
                        hideLoadBar();
                        showError();
                    }
                }

                @Override
                public void onFailure(Call<GetNewComicsesResponse> call, Throwable t) {
                    hideLoadBar();
                    showError();
                }
            });

        } else {
            newComicsesAdapter = new NewComicsesAdapter(main.newComicsesList, fragment, newComicsesViewer);
            newComicsesViewer.setAdapter(newComicsesAdapter);
            hideError();
            hideLoadBar();
        }
    }

    private void showLoadBar(){
        errorLayoutMain.setVisibility(View.VISIBLE);
        loadBarMain.setVisibility(View.VISIBLE);
        httpErrorMain.setVisibility(View.GONE);
    }

    private void hideLoadBar(){
        errorLayoutMain.setVisibility(View.GONE);
        loadBarMain.setVisibility(View.GONE);
    }

    private void showError(){
        errorLayoutMain.setVisibility(View.VISIBLE);
        httpErrorMain.setVisibility(View.VISIBLE);
        loadBarMain.setVisibility(View.GONE);
    }

    private void hideError(){
        errorLayoutMain.setVisibility(View.GONE);
        httpErrorMain.setVisibility(View.GONE);
    }

}
