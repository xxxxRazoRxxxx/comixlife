package com.asprojecteam.comixlife.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.fragment.app.Fragment;

import com.asprojecteam.comixlife.classes.Messages;
import com.asprojecteam.comixlife.comisclife.R;
import com.asprojecteam.comixlife.globals.MainConfig;
import com.asprojecteam.comixlife.interfaces.OnBackPressedListener;

public class FragmentSend extends Fragment implements OnBackPressedListener {

    private Button messageButton;
    private EditText messageText, messageFrom;
    private Spinner messageSubject;
    private String [] messageHints;

    MainConfig main;

    public FragmentSend() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_send, container, false);

        messageButton = view.findViewById(R.id.messageSend);
        messageSubject = view.findViewById(R.id.messageSubject);
        messageFrom = view.findViewById(R.id.messageFrom);
        messageText = view.findViewById(R.id.messageText);

        messageSubject.setOnItemSelectedListener(getOnItemSelectedMessageSubject());
        messageButton.setOnClickListener(getOnClickMessageButton(this));

        messageHints = getResources().getStringArray(R.array.mail_texthint_list);

        main = (MainConfig)  getActivity().getApplicationContext();

        return view;
    }

    private AdapterView.OnItemSelectedListener getOnItemSelectedMessageSubject(){
        AdapterView.OnItemSelectedListener onItemSelectedMessageSubject = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                messageText.setHint(messageHints[i]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        };

        return  onItemSelectedMessageSubject;
    }

    private View.OnClickListener getOnClickMessageButton(final FragmentSend Fragment){
        View.OnClickListener onClickMessageButton = new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if(messageFrom.getText().toString().isEmpty() || messageText.getText().toString().isEmpty())
                    main.showMessage("Заполните все поля");
                else {
                    Messages message = new Messages(Fragment);
                    message.setFrom(messageFrom.getText().toString());
                    message.setText(messageText.getText().toString());
                    message.setSubject(messageSubject.getSelectedItem().toString());
                    message.send();
                }
            }
        };
        return onClickMessageButton;
    }

    @Override
    public Boolean onBackPressed() {

        main.openMainPage(getActivity());
        main.currentFragment = main.FRAGMENT_MAIN;
        return true;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
