package com.asprojecteam.comixlife.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.asprojecteam.comixlife.classes.Comics;
import com.asprojecteam.comixlife.comisclife.R;
import com.github.chrisbanes.photoview.PhotoView;

import java.util.ArrayList;
import java.util.Arrays;

public class ReadComicsFragment extends Fragment {

    static final String ARGUMENT_IMAGE = "arg_image";
    static final String ARGUMENT_PAGE_NUM = "page";

    private String pageImage;
    private Integer pageNum;

    static ViewPager pager;
    static Integer comicsId;
    static Comics comicsRead;

    static SeekBar readSeekBar;

    static ReadComicsFragment newInstance(ViewPager Pager, int Page, Comics Comics, Integer Id, SeekBar ReadSeekBar) {
        pager = Pager;
        comicsId = Id;

        ReadComicsFragment pageFragment = new ReadComicsFragment();
        Bundle arguments = new Bundle();

        comicsRead = Comics;

        String image = new ArrayList<>(Arrays.asList(Comics.pages)).get(Page);

        arguments.putString(ARGUMENT_IMAGE, image);
        arguments.putInt("page", Page);
        pageFragment.setArguments(arguments);
        readSeekBar = ReadSeekBar;
        return pageFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageImage = getArguments().getString(ARGUMENT_IMAGE);
        pageNum = getArguments().getInt(ARGUMENT_PAGE_NUM);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.read_comics_element, null);

        PhotoView photoView = view.findViewById(R.id.photo_view);

        comicsRead.setPageFromUrl(pageNum,pageImage,photoView);

        return view;
    }
}
