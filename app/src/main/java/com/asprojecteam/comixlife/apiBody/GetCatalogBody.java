package com.asprojecteam.comixlife.apiBody;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GetCatalogBody implements Serializable {

    @SerializedName("type")
    private String type;

    @SerializedName("limit")
    private String limit;

    @SerializedName("id")
    private int id;

    public GetCatalogBody(int Id, String Limit) {
        this.type = "getCatalog";
        this.id = Id;
        this.limit = Limit;
    }
}
