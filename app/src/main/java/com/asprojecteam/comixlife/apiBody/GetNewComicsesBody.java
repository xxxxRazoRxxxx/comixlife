package com.asprojecteam.comixlife.apiBody;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GetNewComicsesBody implements Serializable {

    @SerializedName("limit")
    private int limit;

    @SerializedName("type")
    private String type;

    public GetNewComicsesBody(int Limit) {
        this.type = "getNewComicses";
        this.limit = Limit;
    }
}
