package com.asprojecteam.comixlife.apiBody;

import com.google.gson.annotations.SerializedName;

public class GetPagesBody {

    @SerializedName("id")
    private int id;

    @SerializedName("type")
    private String type;

    public GetPagesBody(int Id) {
        this.type = "getPages";
        this.id = Id;
    }
}
