package com.asprojecteam.comixlife.apiBody;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SendEmailBody implements Serializable {

    @SerializedName("type")
    private String type;

    @SerializedName("from")
    private String from;

    @SerializedName("subject")
    private String subject;

    @SerializedName("text")
    private String text;

    public SendEmailBody(String From, String Subject, String Text) {
        this.type = "sendEmail";
        this.from = From;
        this.subject = Subject;
        this.text = Text;
    }
}

