package com.asprojecteam.comixlife.apiBody;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GetPopularBody implements Serializable {

    @SerializedName("limit")
    private int limit;

    @SerializedName("type")
    private String type;

    public GetPopularBody(int Limit) {
        this.type = "getPopularCatalogs";
        this.limit = Limit;
    }
}
