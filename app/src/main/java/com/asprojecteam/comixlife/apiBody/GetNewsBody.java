package com.asprojecteam.comixlife.apiBody;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GetNewsBody implements Serializable {

    @SerializedName("limit")
    private int limit;

    @SerializedName("type")
    private String type;

    public GetNewsBody(int Limit) {
        this.type = "getNews";
        this.limit = Limit;
    }
}
