package com.asprojecteam.comixlife.globals;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.collection.ArrayMap;
import androidx.constraintlayout.widget.Constraints;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

import com.asprojecteam.comixlife.comisclife.R;
import com.bumptech.glide.Glide;
import com.asprojecteam.comixlife.apiInterfaces.ComixLifeApi;
import com.asprojecteam.comixlife.classes.Catalog;
import com.asprojecteam.comixlife.classes.NewComicses;
import com.asprojecteam.comixlife.fragments.FragmentCatalog;
import com.asprojecteam.comixlife.fragments.FragmentMain;
import com.asprojecteam.comixlife.interfaces.ILoadDownloads;
import com.asprojecteam.comixlife.interfaces.ILoadFavorite;
import com.asprojecteam.comixlife.sql.LoadDownloads;
import com.asprojecteam.comixlife.sql.LoadFavorite;
import com.asprojecteam.comixlife.sql.sqlHelper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainConfig extends Application {

    private static long back_pressed;

    public int HTTP_TIMEOUT_ERROR = 400;            // Нет подключения
    public int HTTP_OK = 200;                       // Есть подключение

    public int EXIT_TIME = 2000;                    // 2sec * 1000 mls

    public static int CONNECTION_TIMEOUT = 20;      // Таймаутт подключения

    public int COMICS_OFFSCREEN_PAGE_LIMIT = 2;     // Количество прогружаемых страниц комикса
    public int NEW_COMICSES_LIMIT = 10;                      // Лимит новостей
    public int COMICS_ELEMENT_HEIGHT = 74;          // Примерная высота элемента комикса

    public int APP_OPENED = 0;                      // Приложение открыто
    public int APP_CLOSED = -1;                     // Приложение закрыто

    public int FRAGMENT_MAIN        = 0;
    public int FRAGMENT_CATALOG     = 1;
    public int FRAGMENT_FAVORITE    = 2;
    public int FRAGMENT_DOWNLOADS   = 3;
    public int FRAGMENT_OWNER       = 4;
    public int FRAGMENT_SHARE       = 5;
    public int FRAGMENT_SEND        = 6;

    public int NO_FAVORITES = 0;
    public int OK_FAVORITES = 1;

    public int NO_DOWNLOADS = 0;
    public int OK_DOWNLOADS = 1;

    public int READ_FROM_START = 0;                 // Читать сначала
    public int READ_FROM_CONTINUE = 1;              // Продолжить чтение

    public int EMAIL_HTTP_OK = 5000;                // Запрос отправлен
    public int EMAIL_HTTP_ERROR = 5001;             // Запрос не отправлен

    public String HTTP_ERROR = "Ошибка подключения к интернету. Попробуйте позже.";

    public String MESSAGE_SEND = "Запрос успешно отправлен";
    public String MESSAGE_ERROR = "Во время отправки возникла проблема. Попробуйте позже";
    public String MESSAGE_COMICS_IN_LIST = "Комикс уже в очереди на скачивание";
    public String MESSAGE_COMICS_DOWNLOADED = "Комикс уже был скачен";
    public String MESSAGE_COMICS_ADDED = "Комикс добавлен в очередь на скачивание";

    public String EXIT_MESSAGE = "Нажмите еще раз для Выхода";

    static final String BASE_URL = "http://62.109.25.6/";

    public ComixLifeApi Api;                        // API для работы с сайтом

    public sqlHelper db;                            // Для работы с БД

    public int catalogLimit;                        // Лимит выводимых элементов (комиксов)

    public List<NewComicses> newComicsesList;                     // Массив новостей
    public List<Catalog> popularList;               // Массив популярных комиксов

    public ArrayMap<Integer, Catalog> CatalogList = new ArrayMap<>();   // Массив Всех комиксов

    public int AppState = APP_OPENED;               // Приложение открыто

    public int currentFragment;

    public FragmentMain fragmentMainPage;

    public float appWidth;
    public float appHeight;

    public int openPopularComics = -1;
    public int openNewComics = -1;

    private String name;

    public Integer previousCatalogId = -1;
    public Integer currentCatalogId = -1;

    public String getName() { return name; }

    public void setName(String aName) { name = aName; }

    public void exitApp(FragmentActivity Activity){
        getPopularCatalogLimit();
        if (back_pressed + EXIT_TIME > System.currentTimeMillis()) {
            Activity.finishAffinity();
            AppState = APP_CLOSED;
        }
        else
            showMessage(EXIT_MESSAGE);

        back_pressed = System.currentTimeMillis();
    }

    public void showMessage(String text){
        Toast.makeText(getApplicationContext(),text,Toast.LENGTH_SHORT).show();
    }

    public void showMessage(int num){
        Toast.makeText(getApplicationContext(),num+"",Toast.LENGTH_SHORT).show();
    }

    public int getPopularCatalogLimit(){

        int newComicsHeight = getResources().getDimensionPixelSize(R.dimen.new_comics_height);
        int topLabelHeight = getResources().getDimensionPixelSize(R.dimen.top_label_height)*2;
        int limit = (int)(appHeight-newComicsHeight-topLabelHeight-getStatusBarHeight()-getActionBarHeight())/(int)convertDpToPixel(COMICS_ELEMENT_HEIGHT);
        return limit;
    }

    public String getCatalogLimit(){
        int limit = ((int)(appHeight-getStatusBarHeight()-getActionBarHeight())/(int)convertDpToPixel(COMICS_ELEMENT_HEIGHT))*2;
        catalogLimit = limit;
        String Limit = String.valueOf(limit);
        return Limit;
    }

    public void loadNextCatalog(FragmentCatalog fragmentCatalog, int CatalogId, int lastElement){

        String limitStr = getCatalogLimit();
        Integer limitInt = Integer.parseInt(limitStr);

        limitStr = lastElement + ", " + limitInt;

        this.CatalogList.get(CatalogId).loadCatalogApi(fragmentCatalog, limitStr);
    }

    public void loadCatalog(FragmentCatalog fragmentCatalog, int CatalogId){

        String limitStr = getCatalogLimit();

        if(currentCatalogId != CatalogId){
            previousCatalogId = currentCatalogId;
            currentCatalogId = CatalogId;
        }

        this.CatalogList.get(CatalogId).loadCatalogApi(fragmentCatalog, limitStr);
    }

    public void loadFavorites(Context Context,ILoadFavorite fragmentFavorite){
        LoadFavorite loadFavorite = new LoadFavorite(Context, fragmentFavorite);
        loadFavorite.execute();
    }

    public void loadDownloads(Context Context, ILoadDownloads fragmentDownloads){
        LoadDownloads loadDownloads = new LoadDownloads(Context, fragmentDownloads);
        loadDownloads.execute();
    }

    public LinearLayout.LayoutParams fitDisplay(CardView cv, int elementNumber, int totalItems){

        int newComicsHeight = getResources().getDimensionPixelSize(R.dimen.new_comics_height);
        int topLabelHeight = getResources().getDimensionPixelSize(R.dimen.top_label_height)*2;
        float displaySpace = appHeight-newComicsHeight-topLabelHeight-getStatusBarHeight()-getActionBarHeight();
        int elementHeight = (int)convertDpToPixel(COMICS_ELEMENT_HEIGHT);
        int limit = (int)displaySpace/elementHeight;
        int margin = (int)(displaySpace-(limit*elementHeight))/totalItems;

        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)cv.getLayoutParams();

        if(elementNumber != totalItems-1)
            params.setMargins(1,0,1,margin);

        return params;
    }

    public float convertDpToPixel(float dp){
        Context context = getApplicationContext();
        return dp * ((float) context.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    public void closeLoadComicsPages(){
        //httpGetComicsPages.cancel(true);
    }

    private int getStatusBarHeight(){
        int statusBarHeight = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0)
            statusBarHeight = getResources().getDimensionPixelSize(resourceId);
        return statusBarHeight;
    }

    private int getActionBarHeight(){
        int actionBarHeight = 0;
        TypedValue tv = new TypedValue();
        if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true))
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data,getResources().getDisplayMetrics());
        return actionBarHeight;
    }

    public void openMainPage(FragmentActivity activity){
        FragmentTransaction fragmentTrans = activity.getSupportFragmentManager().beginTransaction();
        fragmentTrans.replace(R.id.main_container, fragmentMainPage);
        fragmentTrans.addToBackStack(null);
        fragmentTrans.commit();
    }

    public void addToCatalogList(List<Catalog> Catalogs){
        for (Catalog catalog: Catalogs) {
            if(CatalogList.get(catalog.getCatalogId()) == null && (catalog.getCatalogType() == null || !catalog.getCatalogType().equals("comics"))){
                CatalogList.put(catalog.getCatalogId(),catalog);
            } else if(CatalogList.get(catalog.getCatalogId()) != null && !catalog.getCatalogType().equals("comics")){
                CatalogList.get(catalog.getCatalogId()).setParentId(catalog.getParentId());
            }
        }
    }

    public void addToCatalogList(Catalog Catalog){
        if(CatalogList.get(Catalog.getCatalogId()) == null && (Catalog.getCatalogType() == null || !Catalog.getCatalogType().equals("comics"))){
            CatalogList.put(Catalog.getCatalogId(),Catalog);
        }
    }

    public byte[] getImageFromUrl(Catalog catalog){

        Context context = getApplicationContext();
        SQLiteDatabase db;
        db = context.openOrCreateDatabase("comics_life.db", Context.MODE_PRIVATE,null);
        db.execSQL("create table if not exists images (id INTEGER PRIMARY KEY autoincrement, type STRING, URL STRING, image blob)");

        Cursor c = db.rawQuery("select image from images WHERE URL='"+catalog.getImageUrl()+"'",null);
        if(!c.moveToFirst()) {
            try {
                catalog.setImageSrc(
                        Glide.with(context)
                        .as(byte[].class)
                        .load(catalog.getImageUrl())
                        .submit()
                        .get()
                );
            } catch (final ExecutionException e) {
                Log.e(Constraints.TAG, e.getMessage());
            } catch (final InterruptedException e) {
                Log.e(Constraints.TAG, e.getMessage());
            }
        }
        else{
            db.close();
            return c.getBlob(0);
        }

        db.close();
        return null;
    }

    public byte[] getImageFromDrawable(){
        Context context = getApplicationContext();
        Resources res = context.getResources();
        Drawable drawable = res.getDrawable(R.drawable.ic_no_picture);
        Bitmap bitmap = ((BitmapDrawable)drawable).getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        return stream.toByteArray();
    }

    public ComixLifeApi getApi() {
        if(this.Api == null){
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            final OkHttpClient getRequestHeader = new OkHttpClient.Builder()
                    .connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                    .writeTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                    .readTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(getRequestHeader)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();

            this.Api = retrofit.create(ComixLifeApi.class);
        }
        return this.Api;
    }
}
