package com.asprojecteam.comixlife.interfaces;

import com.asprojecteam.comixlife.classes.Catalog;

import java.util.List;

public interface ILoadFavorite {
    public void onCompleteLoadFavorite(List<Catalog> Catalog, int Result);
}
