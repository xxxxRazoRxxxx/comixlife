package com.asprojecteam.comixlife.interfaces;

public interface OnBackPressedListener {
    Boolean onBackPressed();
}
