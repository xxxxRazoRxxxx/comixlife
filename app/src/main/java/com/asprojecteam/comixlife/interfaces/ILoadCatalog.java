package com.asprojecteam.comixlife.interfaces;

import com.asprojecteam.comixlife.classes.Catalog;

import java.util.List;

public interface ILoadCatalog {
    public void onCompleteLoadCatalog(List<Catalog> Catalog, Integer httpResult);
}
