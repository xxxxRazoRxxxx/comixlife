package com.asprojecteam.comixlife.interfaces;

import com.asprojecteam.comixlife.classes.Downloads;

import java.util.List;

public interface ILoadDownloads {
    void onCompleteLoadDownloads(List<Downloads> DownloadsList, int Result);
}
