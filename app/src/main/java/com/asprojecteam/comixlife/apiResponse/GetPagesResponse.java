package com.asprojecteam.comixlife.apiResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetPagesResponse {
    @SerializedName("comicses")
    @Expose
    private String[] comicses = null;

    public String[] getComicses() {
        return comicses;
    }

    public void setComicses(String[] comicses) {
        this.comicses = comicses;
    }
}