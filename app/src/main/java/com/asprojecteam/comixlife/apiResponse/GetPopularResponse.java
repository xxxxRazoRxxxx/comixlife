package com.asprojecteam.comixlife.apiResponse;

import com.asprojecteam.comixlife.classes.Catalog;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetPopularResponse {

    @SerializedName("popCatalogs")
    @Expose
    private List<Catalog> popCatalogs = null;

    public List<Catalog> getNewCatalogs() {
        return popCatalogs;
    }

    public void setNewCatalogs(List<Catalog> newCatalogs) {
        this.popCatalogs = newCatalogs;
    }
}
