package com.asprojecteam.comixlife.apiResponse;

import com.asprojecteam.comixlife.classes.NewComicses;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetNewComicsesResponse {

    @SerializedName("newComicses")
    private List<NewComicses> newComicses = null;

    public List<NewComicses> getNewComicses() {
        return newComicses;
    }
}