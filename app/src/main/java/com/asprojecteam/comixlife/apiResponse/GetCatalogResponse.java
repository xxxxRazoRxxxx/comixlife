package com.asprojecteam.comixlife.apiResponse;

import com.asprojecteam.comixlife.classes.Catalog;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetCatalogResponse {
    @SerializedName("catalogs")
    @Expose
    private List<Catalog> catalogs = null;

    public List<Catalog> getCatalogs() {
        return catalogs;
    }

    public void setCatalogs(List<Catalog> catalogs) {
        this.catalogs = catalogs;
    }
}
