package com.asprojecteam.comixlife.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.asprojecteam.comixlife.comisclife.R;
import com.bumptech.glide.Glide;

import com.asprojecteam.comixlife.classes.Catalog;
import com.asprojecteam.comixlife.fragments.FragmentCatalog;
import com.asprojecteam.comixlife.fragments.FragmentMain;
import com.asprojecteam.comixlife.globals.MainConfig;

import java.util.List;

public class RVAdapterPopular extends RecyclerView.Adapter<RVAdapterPopular.CatalogViewHolder>{

    List<Catalog> catalogs;
    FragmentMain fragment;

    static MainConfig main;
    RecyclerView catalogViewer;

    public RVAdapterPopular(List<Catalog> Catalogs, FragmentMain Fragment, RecyclerView CatalogViewer){
        this.catalogs = Catalogs;
        this.fragment = Fragment;
        this.catalogViewer = CatalogViewer;

        main = (MainConfig) this.fragment.getActivity().getApplicationContext();
    }

    public static class CatalogViewHolder extends RecyclerView.ViewHolder {

        public TextView elementName,elementNameEn;
        ImageView elementImage;

        public CardView cv;

        CatalogViewHolder(View itemView) {
            super(itemView);
            cv = itemView.findViewById(R.id.cv);
            elementName = itemView.findViewById(R.id.element_name);
            elementNameEn = itemView.findViewById(R.id.element_name_en);
            elementImage = itemView.findViewById(R.id.element_image);
        }
    }

    @Override
    public int getItemCount() {
        return catalogs.size();
    }

    @Override
    public void onBindViewHolder(RVAdapterPopular.CatalogViewHolder CatalogViewHolder, final int elementNumber) {

        CatalogViewHolder.cv.setLayoutParams(main.fitDisplay(CatalogViewHolder.cv, elementNumber, getItemCount()));

        Catalog catalog = catalogs.get(elementNumber);

        CatalogViewHolder.elementName.setText(catalog.getName());
        CatalogViewHolder.elementNameEn.setText(catalog.getNameEn());

        if(catalog.getImageSrc() != null)
            Glide.with(fragment.getContext()).asBitmap().load(catalog.getImageSrc()).into(CatalogViewHolder.elementImage);
        else
            Glide.with(fragment.getContext()).load(catalog.getImageUrl()).into(CatalogViewHolder.elementImage);

        /*if(catalog.getImageSrc(fragment) != null && catalog.getImageSrc(fragment).length != 0)
            Glide.with(fragment.getContext()).asBitmap().load(catalog.getImageSrc(fragment)).into(CatalogViewHolder.elementImage);
        else
            catalog.setImageView(CatalogViewHolder.elementImage);*/

        CatalogViewHolder.itemView.setOnClickListener(getOnClickCatalogElement(elementNumber,CatalogViewHolder));
    }

    private View.OnClickListener getOnClickCatalogElement(final int elementNumber,final CatalogViewHolder CatalogViewHolder){

        View.OnClickListener OnClickCatalogElement = new View.OnClickListener(){
            @Override
            public void onClick(View v)
            {
                Catalog catalog = catalogs.get(elementNumber);
                FragmentCatalog fragmentCatalog = new FragmentCatalog();

                main.openPopularComics = catalog.getCatalogId();

                FragmentTransaction fragmentTrans = fragment.getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTrans.replace(R.id.main_container, fragmentCatalog);
                fragmentTrans.addToBackStack(null);
                fragmentTrans.commit();
            }
        };
        return OnClickCatalogElement;

    }

    @Override
    public RVAdapterPopular.CatalogViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fragment_main_catalog_element, viewGroup, false);
        return new RVAdapterPopular.CatalogViewHolder(v);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

}
