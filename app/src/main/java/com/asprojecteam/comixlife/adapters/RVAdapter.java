package com.asprojecteam.comixlife.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.asprojecteam.comixlife.comisclife.R;
import com.bumptech.glide.Glide;

import com.asprojecteam.comixlife.classes.Catalog;
import com.asprojecteam.comixlife.fragments.FragmentCatalog;
import com.asprojecteam.comixlife.fragments.FragmentFavorite;
import com.asprojecteam.comixlife.globals.MainConfig;

import java.util.List;

public class RVAdapter extends RecyclerView.Adapter<RVAdapter.CatalogViewHolder>{

    static private int VIEW_TYPE_ITEM = 0;
    static private int VIEW_TYPE_LOAD = 1;

    public List<Catalog> catalogs;
    public int catalogId;
    FragmentCatalog fragmentCatalog;
    FragmentFavorite fragmentFavorite;
    Context fragmentContext;

    MainConfig main;
    RecyclerView catalogViewer;

    public RVAdapter(List<Catalog> Catalogs, FragmentCatalog Fragment, RecyclerView CatalogViewer){
        this.catalogId = Catalogs.get(0).getParentId();
        this.catalogs = Catalogs;
        this.fragmentCatalog = Fragment;
        this.catalogViewer = CatalogViewer;
        this.fragmentContext = fragmentCatalog.getContext();

        if(this.fragmentCatalog.getActivity() != null)
            main = (MainConfig) this.fragmentCatalog.getActivity().getApplicationContext();
    }

    public RVAdapter(List<Catalog> Catalogs, FragmentFavorite Fragment, RecyclerView CatalogViewer){
        this.catalogId = Catalogs.get(0).getParentId();
        this.catalogs = Catalogs;
        this.fragmentFavorite = Fragment;
        this.catalogViewer = CatalogViewer;
        this.fragmentContext = fragmentFavorite.getContext();

        if(this.fragmentFavorite.getActivity() != null)
            main = (MainConfig) this.fragmentFavorite.getActivity().getApplicationContext();
    }

    public static class CatalogViewHolder extends RecyclerView.ViewHolder {

        public TextView elementName,elementNameEn;
        ImageView elementImage;
        LinearLayout cvLL;

        public CardView cv;
        public ProgressBar elementProgressBar,nextPageProgressBar;

        CatalogViewHolder(View itemView, int viewType) {
            super(itemView);

            nextPageProgressBar = itemView.findViewById(R.id.progressBar);
            cv = itemView.findViewById(R.id.cv);
            elementName = itemView.findViewById(R.id.element_name);
            elementNameEn = itemView.findViewById(R.id.element_name_en);

            elementImage = itemView.findViewById(R.id.element_image);
            cvLL = itemView.findViewById(R.id.CardViewLL);

            elementProgressBar = itemView.findViewById(R.id.element_progressBar);
        }
    }

    @Override
    public int getItemCount() {
        return catalogs.size();
    }

    public interface onViewHolderListener{
        void onRequestedLastItem();
    }

    public void removeProgressBar(List<Catalog> Catalogs){
        int num = Catalogs.size();
        if(catalogs.size() > main.catalogLimit){

            if(catalogs.size()-1-num > 0 && getItemViewType(catalogs.size()-1-num) == VIEW_TYPE_LOAD) {
                int n = catalogs.size() - 1 - num;
                catalogs.remove(n);
                notifyItemRangeInserted(catalogs.size()+1,num);
            } else {
                catalogs.remove(catalogs.size()-1);
                catalogs.addAll(Catalogs);
                notifyItemRangeInserted(catalogs.size()+1,Catalogs.size());
            }
        }
    }

    @Override
    public void onBindViewHolder(CatalogViewHolder catalogViewHolder, final int i) {

        Catalog catalog = catalogs.get(i);

        if(catalog.catalogType.equals("next_page")) {

            catalogViewHolder.cv.setVisibility(View.GONE);
            catalogViewHolder.nextPageProgressBar.setVisibility(View.VISIBLE);

            main.loadNextCatalog(fragmentCatalog, main.currentCatalogId, i);
        }

        if(!catalog.getCatalogType().equals("next_page")){

            Context context;

            if(fragmentCatalog != null) {
                context = fragmentCatalog.getContext();
                catalogViewHolder.itemView.setOnClickListener(getOnClickCatalogElement(i, catalogViewHolder));
            } else {
                context = catalog.getContext();
                catalogViewHolder.itemView.setOnClickListener(getOnClickFavoriteElement(i, catalogViewHolder));
            }

            catalogViewHolder.elementName.setText(catalog.getName());
            catalogViewHolder.elementNameEn.setText(catalog.getNameEn());

            if(catalog.getImageSrc() != null)
                Glide.with(context).asBitmap().load(catalog.getImageSrc()).into(catalogViewHolder.elementImage);
            else
                Glide.with(context).load(catalog.getImageUrl()).into(catalogViewHolder.elementImage);

            /*if(catalog.getImageSrc(fragmentCatalog) != null && catalog.getImageSrc(fragmentCatalog).length != 0)
                Glide.with(fragmentCatalog.getContext()).asBitmap().load(catalog.getImageSrc(fragmentCatalog)).apply(new RequestOptions()
                        .placeholder(R.drawable.ic_no_picture)).into(catalogViewHolder.elementImage);
            else
                catalog.setImageView(catalogViewHolder.elementImage);*/

            /*if(catalog.getImageSrc() != null && catalog.getImageSrc(fragmentCatalog).length != 0)
                Glide.with(fragmentContext)
                        .asBitmap()
                        .load(catalog.getImageSrc(fragmentCatalog))
                        .apply(new RequestOptions()
                                .placeholder(R.drawable.ic_no_picture))
                        .into(catalogViewHolder.elementImage);
            else
                catalog.setImageView(catalogViewHolder.elementImage);*/
        }

    }

    private View.OnClickListener getOnClickCatalogElement(final int i,final CatalogViewHolder catalogViewHolder){

        View.OnClickListener OnClickCatalogElement = new View.OnClickListener(){
            @Override
            public void onClick(View v)
            {
                if(fragmentCatalog.inLoad)
                    return;

                if(fragmentCatalog.inLoadComicsInfo)
                    return;

                Catalog catalog = catalogs.get(i);

                if(catalog.getCatalogType().equals("catalog")){

                    saveCatalogInstance();
                    fragmentCatalog.publishId = catalog.getCatalogId();
                    fragmentCatalog.inLoad = true;
                    catalogViewHolder.elementProgressBar.setVisibility(View.VISIBLE);

                    main.loadCatalog(fragmentCatalog, catalog.getCatalogId());

                }else if(catalog.getCatalogType().equals("comics")){

                    saveCatalogInstance();
                    catalog.loadComicsPages(fragmentCatalog, main.READ_FROM_START);
                }
            }
        };
        return OnClickCatalogElement;
    }

    private View.OnClickListener getOnClickFavoriteElement(final int i,final CatalogViewHolder catalogViewHolder){

        View.OnClickListener OnClickCatalogElement = new View.OnClickListener(){
            @Override
            public void onClick(View v)
            {
                Catalog catalog = catalogs.get(i);
                FragmentCatalog fragmentCatalog = new FragmentCatalog();

                main.openPopularComics = catalog.getCatalogId();

                FragmentTransaction fragmentTrans = fragmentFavorite.getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTrans.replace(R.id.main_container, fragmentCatalog);
                fragmentTrans.addToBackStack(null);
                fragmentTrans.commit();
            }
        };
        return OnClickCatalogElement;

    }

    @Override
    public int getItemViewType(int position) {
        if (catalogs.get(position).getCatalogType().equals("next_page"))
            return VIEW_TYPE_LOAD;
        else
            return VIEW_TYPE_ITEM;
    }

    @Override
    public CatalogViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fragment_catalog_element, viewGroup, false);
        return new CatalogViewHolder(v, viewType);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void saveCatalogInstance(){
        main.CatalogList.get(catalogs.get(0).getParentId()).setSavedInstanceState(catalogViewer.getLayoutManager().onSaveInstanceState());
    }
}