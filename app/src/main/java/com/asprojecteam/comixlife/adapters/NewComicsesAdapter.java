package com.asprojecteam.comixlife.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.asprojecteam.comixlife.comisclife.R;
import com.bumptech.glide.Glide;

import com.asprojecteam.comixlife.classes.NewComicses;
import com.asprojecteam.comixlife.fragments.FragmentMain;
import com.asprojecteam.comixlife.globals.MainConfig;

import java.util.List;

public class NewComicsesAdapter  extends RecyclerView.Adapter<NewComicsesAdapter.NewComicsViewHolder> {

    List<NewComicses> newComicses;
    FragmentMain fragment;

    static MainConfig main;
    RecyclerView newComicsesViewer;

    public NewComicsesAdapter(List<NewComicses> NewComicses, FragmentMain Fragment, RecyclerView NewComicsesViewer){
        this.newComicses = NewComicses;
        this.fragment = Fragment;
        this.newComicsesViewer = NewComicsesViewer;

        main = (MainConfig) this.fragment.getActivity().getApplicationContext();
    }

    public static class NewComicsViewHolder extends RecyclerView.ViewHolder {

        public TextView newComicsName;
        ImageButton newComicsImage;
        ImageView newComicsLabel;

        public CardView newComicsCardView;

        NewComicsViewHolder(View itemView) {
            super(itemView);
            newComicsCardView = itemView.findViewById(R.id.new_comics_card_view);
            newComicsImage = itemView.findViewById(R.id.new_comics_image);
            newComicsLabel = itemView.findViewById(R.id.new_comics_label);
            newComicsName = itemView.findViewById(R.id.new_comics_name);
        }
    }

    @NonNull
    @Override
    public NewComicsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fragment_main_new_comicses_element, viewGroup, false);
        return new NewComicsesAdapter.NewComicsViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull NewComicsViewHolder newComicsViewHolder, int i) {
        NewComicses newComics = newComicses.get(i);

        if(newComics.isNewComicsLabel())
            newComicsViewHolder.newComicsLabel.setVisibility(View.VISIBLE);

        newComicsViewHolder.newComicsName.setText(newComics.getName());
        newComicsViewHolder.newComicsImage.setOnClickListener(newComics.getOnClickNewComics(fragment, main));
        Glide.with(fragment.getContext()).load(newComics.getImageUrl()).fitCenter().placeholder(R.drawable.ic_no_picture).into(newComicsViewHolder.newComicsImage);
    }

    @Override
    public int getItemCount() {
        return newComicses.size();
    }
}
