package com.asprojecteam.comixlife.classes;

import android.app.Activity;

import com.asprojecteam.comixlife.apiBody.SendEmailBody;
import com.asprojecteam.comixlife.apiResponse.SendEmailResponse;
import com.asprojecteam.comixlife.fragments.FragmentSend;
import com.asprojecteam.comixlife.globals.MainConfig;
import com.asprojecteam.comixlife.apiInterfaces.ComixLifeApi;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Messages {

    MainConfig main;
    Activity activity;

    private String from, subject, text;
    private ComixLifeApi comixLifeApi;

    public Messages(FragmentSend Fragment){
        if((activity = Fragment.getActivity()) != null)
            main = (MainConfig) activity.getApplicationContext();

        this.comixLifeApi = main.getApi();
    }

    public void setFrom(String From){
        this.from = From;
    }

    public void setSubject(String Subject){
        this.subject = Subject;
    }

    public void setText(String Text){
        this.text = Text;
    }

    public void send(){
        SendEmailBody mess = new SendEmailBody(this.from, this.subject, this.text);
        this.comixLifeApi.sendEmail(mess).enqueue(new Callback<SendEmailResponse>() {
            @Override
            public void onResponse(Call<SendEmailResponse> call, Response<SendEmailResponse> response) {
                if(response.isSuccessful() && response.body().getResult() == main.EMAIL_HTTP_OK){
                    main.showMessage(main.MESSAGE_SEND);
                } else {
                    main.showMessage(main.MESSAGE_ERROR);
                }
            }

            @Override
            public void onFailure(Call<SendEmailResponse> call, Throwable t) {
                main.showMessage(main.MESSAGE_ERROR);
            }
        });
    }
}