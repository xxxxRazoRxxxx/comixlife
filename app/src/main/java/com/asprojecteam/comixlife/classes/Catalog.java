package com.asprojecteam.comixlife.classes;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.asprojecteam.comixlife.adapters.RVAdapter;
import com.asprojecteam.comixlife.apiBody.GetCatalogBody;
import com.asprojecteam.comixlife.apiBody.GetPagesBody;
import com.asprojecteam.comixlife.apiResponse.GetCatalogResponse;
import com.asprojecteam.comixlife.apiResponse.GetPagesResponse;
import com.asprojecteam.comixlife.comisclife.R;
import com.asprojecteam.comixlife.fragments.FragmentCatalog;
import com.asprojecteam.comixlife.fragments.FragmentMain;
import com.asprojecteam.comixlife.globals.MainConfig;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Catalog {

    @SerializedName("catalog_id")
    @Expose
    private int catalogId;
    @SerializedName("catalog_type")
    @Expose
    public String catalogType;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("name_en")
    @Expose
    private String nameEn;
    @SerializedName("image_url")
    @Expose
    private String imageUrl;
    @SerializedName("year")
    @Expose
    private String year;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("number")
    @Expose
    private String number;
    @SerializedName("localization")
    @Expose
    private String localization;
    @SerializedName("parent_id")
    @Expose
    private int parentId;

    private ImageView imageView;
    private byte[] imageSrc;
    private List<Catalog> children = new ArrayList<>();
    private Parcelable savedInstanceState;
    private boolean inFavorite = false;
    private boolean inDownloadList = false;
    private boolean downloaded = false;
    private Context context;

    static MainConfig main;

    public Catalog( Context Context, int CatalogId) {

        this.catalogId = CatalogId;
        this.parentId = -1;
        this.context = Context;

        main = (MainConfig) Context.getApplicationContext();

        checkFavorite();
        checkDownloaded();
    }

    public int getCatalogId() {
        return catalogId;
    }

    public void setCatalogId(int catalogId) {
        this.catalogId = catalogId;
    }

    public String getCatalogType() {
        return catalogType;
    }

    public void setCatalogType(String catalogType) {
        this.catalogType = catalogType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getLocalization() {
        return localization;
    }

    public void setLocalization(String localization) {
        this.localization = localization;
    }

    public ImageView getImageView() {
        return imageView;
    }

    public void setImageView(ImageView imageView) {
        this.imageView = imageView;
    }

    public byte[] getImageSrc(FragmentMain fragment) {

        Activity activity;

        if(imageSrc == null && (activity = fragment.getActivity()) != null) {
            main = (MainConfig) activity.getApplicationContext();

            if (!this.imageUrl.isEmpty())
                imageSrc = main.getImageFromUrl( this);
            else
                imageSrc = main.getImageFromDrawable();
        }

        return imageSrc;
    }

    public byte[] getImageSrc(FragmentCatalog fragment) {

        Activity activity;

        if(imageSrc == null && (activity = fragment.getActivity()) != null) {
            main = (MainConfig) activity.getApplicationContext();

            if (!this.imageUrl.isEmpty())
                imageSrc = main.getImageFromUrl(this);
            else
                imageSrc = main.getImageFromDrawable();
        }

        return imageSrc;
    }

    public byte[] getImageSrc() {
        return imageSrc;
    }

    public void setImageSrc(byte[] imageSrc) {
        this.imageSrc = imageSrc;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public void createChildren(List<Catalog> Children){
        children = Children;
    }

    public void addChildren(List<Catalog> Children, RVAdapter rvAdapter){

        if(children == null)
            children = new ArrayList<>();

        children.addAll(Children);

        if(rvAdapter != null)
            rvAdapter.removeProgressBar(Children);
    }

    public void loadChildren(FragmentCatalog fragment){

        Activity activity;

        if((activity = fragment.getActivity()) != null) {
            main = (MainConfig) activity.getApplicationContext();
            main.loadCatalog(fragment, catalogId);
        }
    }

    public void setSavedInstanceState(Parcelable savedInstanceState) {
        this.savedInstanceState = savedInstanceState;
    }

    public Parcelable getSavedInstanceState() {
        return savedInstanceState;
    }

    public List<Catalog> getChildren() {
        return children;
    }

    public boolean isInDownloadList() {
        return inDownloadList;
    }

    public boolean isInFavorite() {
        return inFavorite;
    }

    public void setInDownloadList(boolean inDownloadList) {
        this.inDownloadList = inDownloadList;
    }

    public void setInFavorite(boolean inFavorite) {
        this.inFavorite = inFavorite;
    }

    public void setContext(Context Context){this.context = Context;}

    public Context getContext(){return context;}

    public View.OnClickListener onClickFavoriteButton(final ImageButton favoriteButton){
        final View.OnClickListener FavoriteButton = new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if(!isInFavorite()) {
                    addToFavorite();
                    favoriteButton.setImageResource(R.drawable.icon_favorite);
                } else {
                    deleteFromFavorite();
                    favoriteButton.setImageResource(R.drawable.icon_favorite_border);
                }
            }
        };
        return FavoriteButton;
    }

    public View.OnClickListener onClickDownloadButton(final ImageButton downloadButton){
        final View.OnClickListener DownloadButton = new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if(downloaded)
                    main.showMessage(main.MESSAGE_COMICS_DOWNLOADED);
                if(!isInDownloadList()) {
                    addToDownloadList();
                    downloadButton.setVisibility(View.GONE);
                    main.showMessage(main.MESSAGE_COMICS_ADDED);
                }
                else
                    main.showMessage(main.MESSAGE_COMICS_IN_LIST);
            }
        };
        return DownloadButton;
    }


    public void addToFavorite(){
        SQLiteDatabase db;
        db = context.openOrCreateDatabase("comics_life.db", Context.MODE_PRIVATE,null);

        db.execSQL("create table if not exists favorite (id INTEGER PRIMARY KEY autoincrement, catalog_id INTEGER, name STRING, name_en STRING, image_url STRING, description STRING, catalog_type STRING, year STRING, parent_id INTEGER, translateBy STRING)");

        Cursor c = db.rawQuery("select catalog_id from favorite WHERE catalog_id="+catalogId,null);

        if(!c.moveToFirst()) {
            ContentValues values = new ContentValues();
            values.put("catalog_id", catalogId);
            values.put("name", name);
            values.put("name_en", nameEn);
            values.put("image_url", imageUrl);
            values.put("description", description);
            values.put("catalog_type", catalogType);
            values.put("year", year);
            values.put("parent_id", parentId);
            values.put("translateBy", localization);

            db.insert("favorite", null, values);
        }

        db.close();

        this.inFavorite = true;
    }

    public void deleteFromFavorite(){
        SQLiteDatabase db;
        db = context.openOrCreateDatabase("comics_life.db", Context.MODE_PRIVATE,null);
        db.delete("favorite", "catalog_id = ?", new String[]{String.valueOf(catalogId)});
        db.close();

        this.inFavorite = false;
    }

    public void addToDownloadList(){
        SQLiteDatabase db;
        db = context.openOrCreateDatabase("comics_life.db", Context.MODE_PRIVATE,null);

        Cursor c = db.rawQuery("select id from downloads_list WHERE catalog_id="+catalogId,null);

        if(!c.moveToFirst()) {
            ContentValues values = new ContentValues();
            values.put("catalog_id", catalogId);
            values.put("url", "");                              //TODO: Need to change URL
            values.put("status", "N");

            db.insert("downloads_list", null, values);
        }

        db.close();

        this.inDownloadList = true;
    }

    public boolean checkFavorite(){
        SQLiteDatabase db;
        db = context.openOrCreateDatabase("comics_life.db", Context.MODE_PRIVATE,null);

        Cursor c = db.rawQuery("select * from favorite WHERE catalog_id="+catalogId,null);

        if(c.moveToFirst())
            inFavorite = true;
        else
            inFavorite = false;

        db.close();
        return inFavorite;
    }

    public void checkDownloaded(){
        SQLiteDatabase db;
        db = context.openOrCreateDatabase("comics_life.db", Context.MODE_PRIVATE,null);

        Cursor c = db.rawQuery("select * from downloads_list WHERE catalog_id="+catalogId,null);

        if(c.moveToFirst()) {
            this.inDownloadList = true;
            if (c.getString(3).equals("Y"))
                this.downloaded = true;
        }

        db.close();
    }

    public void loadCatalogApi(final FragmentCatalog fragmentCatalog, final String limitStr){
        GetCatalogBody getCatalogBody = new GetCatalogBody(this.catalogId, limitStr);

        main.getApi().getCatalog(getCatalogBody).enqueue(new Callback<GetCatalogResponse>() {
            @Override
            public void onResponse(Call<GetCatalogResponse> call, Response<GetCatalogResponse> response) {
                if(response.isSuccessful() && response.body() != null){

                    fragmentCatalog.inLoad = false;

                    fragmentCatalog.hideLoadBar();
                    fragmentCatalog.hideError();

                    List<Catalog> catalogs = response.body().getCatalogs();

                    if (catalogs.size() <= 0)
                        fragmentCatalog.showEmptyError();
                    else
                        fragmentCatalog.showCatalog(catalogs);
                } else {
                    fragmentCatalog.hideLoadBar();
                    fragmentCatalog.showError();
                }
            }

            @Override
            public void onFailure(Call<GetCatalogResponse> call, Throwable t) {
                fragmentCatalog.hideLoadBar();
                fragmentCatalog.showError();
            }
        });
    }

    public void loadComicsPages(final FragmentCatalog FragmentCatalog, final int StartType){

        GetPagesBody getPagesBody = new GetPagesBody(this.catalogId);

        final int Id = this.catalogId;

        main.getApi().getPages(getPagesBody).enqueue(new Callback<GetPagesResponse>() {
            @Override
            public void onResponse(Call<GetPagesResponse> call, Response<GetPagesResponse> response) {
                if(response.isSuccessful() && response.body() != null){
                    response.body().getComicses();
                    Comics comics = new Comics(main.getApplicationContext(), response.body().getComicses());
                    FragmentCatalog.inRead = true;
                    FragmentCatalog.openComicsPages(comics, Id, StartType);
                } else {
                    main.showMessage(main.HTTP_ERROR);
                    FragmentCatalog.inRead = false;
                }
            }

            @Override
            public void onFailure(Call<GetPagesResponse> call, Throwable t) {
                main.showMessage(main.HTTP_ERROR);
                FragmentCatalog.inRead = false;
            }
        });
    }
}
