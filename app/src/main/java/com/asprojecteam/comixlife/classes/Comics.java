package com.asprojecteam.comixlife.classes;

import android.content.Context;
import android.widget.ImageView;
import android.widget.SeekBar;

import androidx.collection.ArrayMap;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;

public class Comics {

    public String[] pages;
    public Context context;
    public ViewPager pager;

    public int currentPage;

    public ArrayMap<Integer, byte[]> pagesSource = new ArrayMap<>();

    public Comics(Context Context,String[] Pages) {
        this.context = Context;
        this.pages = Pages;
    }

    public void setPageFromUrl(Integer Num, String Page,ImageView PageView){

        this.currentPage = pager.getCurrentItem();

        if(this.pagesSource.containsKey(Num))
            Glide.with(this.context)
                    .asBitmap()
                    .load(this.pagesSource.get(Num))
                    .into(PageView);
        else {
            Glide.with(this.context)
                    .asBitmap()
                    .load(Page)
                    .into(PageView);
        }
    }

    public ViewPager.OnPageChangeListener getPageChangeListener(final SeekBar ReadSeekBar){
        return new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                ReadSeekBar.setProgress(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        };
    }

    public SeekBar.OnSeekBarChangeListener getReadSeekBarChangeListner(){
        return new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                pager.setCurrentItem(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        };
    }
}
