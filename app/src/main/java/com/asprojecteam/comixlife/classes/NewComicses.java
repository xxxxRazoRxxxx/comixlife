package com.asprojecteam.comixlife.classes;

import android.view.View;

import androidx.fragment.app.FragmentTransaction;

import com.asprojecteam.comixlife.comisclife.R;
import com.asprojecteam.comixlife.fragments.FragmentCatalog;
import com.asprojecteam.comixlife.fragments.FragmentMain;
import com.asprojecteam.comixlife.globals.MainConfig;

import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Date;

public class NewComicses {

    @SerializedName("comics_id")
    private int comicsId;
    @SerializedName("name")
    private String name;
    @SerializedName("timestamp_added")
    private String date;
    @SerializedName("image_url")
    private String imageUrl;
    @SerializedName("new")
    private boolean newComicsLabel;

    public int getComicsId() {
        return comicsId;
    }

    public String getName() {
        return name;
    }

    public String getDate() {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        return format.format(new Date(Long.parseLong(this.date)*1000));
    }

    public boolean isNewComicsLabel(){
        return newComicsLabel;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public View.OnClickListener getOnClickNewComics(final FragmentMain fragmentMain, final MainConfig main){
        final NewComicses newComics = this;

        View.OnClickListener OnClickNewComics = new View.OnClickListener(){
            @Override
            public void onClick(View v)
            {

                FragmentCatalog fragmentCatalog = new FragmentCatalog();

                main.openNewComics = newComics.getComicsId();

                FragmentTransaction fragmentTrans = fragmentMain.getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTrans.replace(R.id.main_container, fragmentCatalog);
                fragmentTrans.addToBackStack(null);
                fragmentTrans.commit();
            }
        };
        return OnClickNewComics;
    }
}
